package com.geekhome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Copyright © 2019年 zn-security. All rights reserved.
 *
 * @author 临江仙 hzqiuxm@163.com
 * 启动类
 * @date 2019/4/29 19:37
 * <p>

 */
@SpringBootApplication
public class ZnSecurityApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZnSecurityApplication.class, args);
    }
}
