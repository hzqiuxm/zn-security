package com.geekhome.gateways.controller;

import com.geekhome.global.common.BaseController;
import com.geekhome.global.common.JsonResult;
import com.geekhome.global.common.ZnxzExceptioon;
import com.geekhome.domain.entity.User;
import com.geekhome.domain.respository.UserRepository;
import com.geekhome.gateways.controller.query.UserQueryCondition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Copyright © 2019年 zn-security. All rights reserved.
 *
 * @author 临江仙 hzqiuxm@163.com
 * 用户控制类
 * @date 2019/4/30 15:04
 * <p>

 */
@RestController
@Slf4j
@RequestMapping("/users")
public class UserController extends BaseController {


    @Autowired
    private UserRepository userRepository;


    @GetMapping("/")
    public JsonResult findAllUser(){

        List<User> usersAll = userRepository.findAll();

        usersAll.forEach(System.out::println);

        return getSuccessResult(usersAll);

    }


    @GetMapping("/person_details")
    public HttpEntity<List<User>> findByCondition(@Validated UserQueryCondition userQueryCondition){


        System.out.println(userQueryCondition);

        checkUserCondition(userQueryCondition);

        List<User> usersAll = userRepository.findAll();

        return new ResponseEntity<>(usersAll,HttpStatus.OK);

    }

    private void checkUserCondition(UserQueryCondition userQueryCondition) {

        System.out.println("---------------1-----------------");
        if(userQueryCondition.getUserName().equals("毛泽东")){
            System.out.println("-----------------2---------------");
            throw new ZnxzExceptioon(211,"名字中含有敏感词汇");
        }
    }

}
