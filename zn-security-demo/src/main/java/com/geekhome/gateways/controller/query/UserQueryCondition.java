package com.geekhome.gateways.controller.query;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * Copyright © 2019年 zn-security. All rights reserved.
 *
 * @author 临江仙 hzqiuxm@163.com
 * 用户相关查询条件类
 * @date 2019/8/24 12:01
 */
@Getter
@Setter
@ToString
public class UserQueryCondition {

    @NotBlank(message = "用户名不能为空")
    private String userName;

    private String phone = "13989461462";

    @Min(value = 0,message = "age参数的值必须大于0")
    @Max(value = 100,message = "age参数的值不能大于100")
    private int  age;
}
