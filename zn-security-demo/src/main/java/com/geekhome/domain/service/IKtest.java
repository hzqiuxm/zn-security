package com.geekhome.domain.service;

import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import java.io.StringReader;
import java.util.HashSet;
import java.util.Set;

/**
 * Copyright © 2019年 zn-security. All rights reserved.
 *
 * @author 临江仙 hzqiuxm@163.com
 * IK测试
 * @date 2019/5/6 17:26
 * <p>

 */
public class IKtest {


    public static void main(String[] args) {


        IKtest iKtest = new IKtest();
        Set<String> stringSet = iKtest.splitKeyword("发芽的马铃薯可以吃吗？");

        System.out.println(stringSet);

    }

    private Set<String> splitKeyword(String content){
        Set<String> set = new HashSet<>();
        StringReader reader = new StringReader(content);
        IKSegmenter ik = new IKSegmenter(reader, true);
        Lexeme lex;
        try {
            while ((lex = ik.next()) != null) {
                if (lex.getLexemeText().length() > 1) {
                    set.add(lex.getLexemeText());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return set;
    }
}
