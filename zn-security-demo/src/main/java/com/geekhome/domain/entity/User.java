package com.geekhome.domain.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Copyright © 2019年 zn-security. All rights reserved.
 *
 * @author 临江仙 hzqiuxm@163.com
 * 用户类
 * @date 2019/4/30 15:16
 * <p>

 */
@Data
@Entity
public class User {
    @Id
    private Integer id;

    private String userName;

    private String password;

    public User() {
    }
}
