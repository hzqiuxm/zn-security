package com.geekhome.domain.respository;


import com.geekhome.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Copyright © 2019年 zn-security. All rights reserved.
 *
 * @author 临江仙 hzqiuxm@163.com
 * 用户持久化接口
 * @date 2019/4/30 15:20
 * <p>
 */
public interface UserRepository extends JpaRepository<User,Integer> {

}
