package com.geekhome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Copyright © 2019年 zn-security. All rights reserved.
 *
 * @author 临江仙 hzqiuxm@163.com
 * demo启动类,
 * exclude = SecurityAutoConfiguration.class  禁用springsecurity的默认用户名和密码  为什么不起作用？单项目是可以的
 * @date 2019/4/30 10:47
 * <p>

 */
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@RestController
@EnableJpaRepositories
public class DemoApplication {

    public static void main(String[] args) {

        SpringApplication.run(DemoApplication.class,args);
    }


    @GetMapping("/hello")
    public String hello(){

        return "hello spring security";
    }
}
