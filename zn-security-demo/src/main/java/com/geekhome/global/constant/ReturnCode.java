package com.geekhome.global.constant;

/**
 * Copyright © 2019年 com.geekhome.znxz. All rights reserved.
 *
 * @author 临江仙 hzqiuxm@163.com
 * 返回值
 * @date 2019/4/27 16:17
 * <p>

 */
public enum ReturnCode {
    /**
     * 成功及失败code定义
     */
    SUCCESS_CODE("SUCCESS_CODE", 200), UNDEFINED_ERROR("UNDEFINED_ERROR", 520);
    private int codeNum;

    public int getCodeNum() {
        return codeNum;
    }

    ReturnCode(String codeName, int codeNum) {
        this.codeNum = codeNum;
    }
}
