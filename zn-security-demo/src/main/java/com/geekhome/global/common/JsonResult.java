package com.geekhome.global.common;

import lombok.ToString;

/**
 * Copyright © 2019年 com.geekhome.znxz. All rights reserved.
 *
 * @author 临江仙 hzqiuxm@163.com
 * 通用数据返回
 * @date 2019/4/27 16:14
 * <p>

 */
@ToString
public class JsonResult<T> {

    private int code;
    private Integer subCode;
    private String msg;
    private T data;

    public JsonResult(int code, int subCode, String msg){
        this.code = code;
        this.subCode = subCode;
        this.msg = msg;
    }

    public JsonResult(int code, String msg, T data){
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public JsonResult(){

    }
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Integer getSubCode() {
        return subCode;
    }

    public void setSubCode(Integer subCode) {
        this.subCode = subCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
