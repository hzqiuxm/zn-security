package com.geekhome.global.common;

import com.geekhome.global.constant.ReturnCode;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright © 2019年 com.geekhome.znxz. All rights reserved.
 *
 * @author 临江仙 hzqiuxm@163.com
 * 公共控制器，实现基本返回方法封装
 * @date 2019/4/27 16:13
 * <p>

 */
@Slf4j
public class BaseController {
    /**
     * 没有其他数据时的成功返回
     *
     * @return 返回一个成功的ResponseObject对象
     */
    protected JsonResult getSuccessResult() {
        JsonResult response = new JsonResult();
        generateSuccessResult(response);
        return response;
    }

    /**
     * 只有
     *
     * @param data 成功时返回的数据
     * @return 返回一个成功的ResponseObject对象
     */
    protected JsonResult getSuccessResult(Object data) {
        JsonResult response = new JsonResult();
        generateSuccessResult(response);
        response.setData(data);
        return response;
    }


    protected JsonResult getErrorResult(int subCode, String msg) {
        JsonResult response = new JsonResult();
        generateErrorResult(response);
        response.setSubCode(subCode);
        response.setMsg(msg);
        return response;
    }


    /**
     * 生成一个正确返回的ResponseObject
     */
    private void generateSuccessResult(JsonResult response) {
        response.setCode(ReturnCode.SUCCESS_CODE.getCodeNum());
        response.setMsg("成功");
    }

    /**
     * 生成一个报错返回的ResponseObject
     */
    private void generateErrorResult(JsonResult response) {
        response.setCode(ReturnCode.UNDEFINED_ERROR.getCodeNum());
        response.setMsg("失败");
    }

    /*@ExceptionHandler
    public JsonResult handingException(Exception ex) {
        log.error(ex.getMessage(), ex);
        //@Validated注解在类上
        if (ex instanceof ConstraintViolationException) {
            final StringBuilder sbf = new StringBuilder();
            ((ConstraintViolationException) ex).getConstraintViolations()
                    .forEach(e -> sbf.append(e.getMessage()).append(","));
            return getErrorResult(10000, sbf.deleteCharAt(sbf.length() - 1).toString());
        }

        //bind方式校验参数产生异常
        if (ex instanceof BindException) {
            final StringBuilder sb = new StringBuilder();
            ((BindException) ex).getAllErrors().forEach(e -> sb.append(e.getDefaultMessage())
                    .append(","));
            return getErrorResult(10000, sb.deleteCharAt(sb.length() - 1).toString());
        }

        //@Validated注解在方法上产生异常
        if (ex instanceof MethodArgumentNotValidException) {
            final StringBuilder sb = new StringBuilder();
            ((MethodArgumentNotValidException) ex).getBindingResult().getAllErrors()
                    .forEach(e -> sb.append(e.getDefaultMessage()).append(","));
            return getErrorResult(10000, sb.deleteCharAt(sb.length() - 1).toString());
        }

        if (ex instanceof ZnxzExceptioon) {
            return getErrorResult(((ZnxzExceptioon) ex).getCode(), ex.getMessage());
        }
        return getErrorResult(520, "系统异常");
    }*/
}
