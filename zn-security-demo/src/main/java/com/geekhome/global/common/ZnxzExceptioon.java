package com.geekhome.global.common;

/**
 * Copyright © 2019年 com.geekhome.znxz. All rights reserved.
 *
 * @author 临江仙 hzqiuxm@163.com
 * 项目系统类
 * @date 2019/4/27 16:23
 * <p>
 */
public class ZnxzExceptioon extends RuntimeException{
    protected int code;
    protected String message;

    /**
     * 异常构造方法，必须有Code和msg
     * @param code 状态码
     * @param message  错误信息
     */
    public ZnxzExceptioon(int code, String message){
        super();
        this.code = code;
        this.message = message;
    }

    public int getCode(){
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
